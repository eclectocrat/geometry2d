#ifndef _ec_geometry2d_hpp
#define _ec_geometry2d_hpp

#include <cmath>
#include <limits>

namespace ec {
template <typename T> struct Point {
    T x = 0;
    T y = 0;

    constexpr Point() noexcept = default;
    constexpr Point(T x_, T y_) noexcept : x(x_), y(y_) {}
    template <typename U> constexpr explicit Point(Point<U> p): x(p.x), y(p.y) {}

    void operator+=(Point const &p) { x += p.x; y += p.y; }
    void operator-=(Point const &p) { x -= p.x; y -= p.y; }
    constexpr Point operator-() const { return Point(-x, -y); }

    template <typename Size> constexpr Point translated(Size s) const { return Point(x + s.w, y + s.h); }
};

template <typename T> constexpr bool operator==(Point<T> const &p1, Point<T> const &p2) { return p1.x == p2.x and p1.y == p2.y; }
template <typename T> constexpr bool operator!=(Point<T> const &p1, Point<T> const &p2) { return p1.x != p2.x or p1.y != p2.y; }
template <typename T> constexpr auto operator+ (Point<T> const &p1, Point<T> const &p2) { return Point<T>(p1.x + p2.x, p1.y + p2.y); }
template <typename T> constexpr auto operator- (Point<T> const &p1, Point<T> const &p2) { return Point<T>(p1.x - p2.x, p1.y - p2.y); }
template <typename T> constexpr auto operator* (Point<T> const &p, T m) { return Point<T>(p.x * m, p.y * m); }
template <typename T> constexpr auto operator/ (Point<T> const &p, T m) { return Point<T>(p.x / m, p.y / m); }
template <typename T> constexpr auto min_y(Point<T> const &p1, Point<T> const &p2) { return p1.y <= p2.y ? p1 : p2; }
template <typename T> constexpr auto max_y(Point<T> const &p1, Point<T> const &p2) { return p1.y >= p2.y ? p1 : p2; }
template <typename T> constexpr auto min_x(Point<T> const &p1, Point<T> const &p2) { return p1.x <= p2.x ? p1 : p2; }
template <typename T> constexpr auto max_x(Point<T> const &p1, Point<T> const &p2) { return p1.x >= p2.x ? p1 : p2; }

template <typename T> constexpr double slope(Point<T> p1, Point<T> p2) {
    const auto d = (p2.x - p1.x);
    return d ? (p2.y - p1.y) / d : std::numeric_limits<float>::max();
}

template <typename T>
constexpr bool operator<(Point<T> const &p1, Point<T> const &p2) {
    if (p1.y == p2.y) {
        return p1.x < p2.x;
    } else {
        return p1.y < p2.y;
    }
}

template <typename T>
constexpr double linear_distance(Point<T> p1, Point<T> p2) {
    const double dx = std::fabs(p1.x - p2.x);
    const double dy = std::fabs(p1.y - p2.y);
    return std::sqrt(std::pow(dx, 2) + std::pow(dy, 2));
}

template <typename T> auto abs(Point<T> const &p) {
    return Point<T>(std::abs(p.x), std::abs(p.y));
}

template <typename T = size_t> struct Size {
    T w = 0;
    T h = 0;

    constexpr Size() noexcept = default;
    constexpr Size(T w_, T h_) noexcept : w(w_), h(h_) {}
    template <typename U> constexpr explicit Size(Size<U> s) : w(s.w), h(s.h) {}

    constexpr T area() const { return w * h; }
    constexpr bool contains_size(const Size s) const { return w >= s.w and h >= s.h; }
    constexpr bool contains_point(const Point<T> p) const noexcept;

    void operator+=(Size sz) { w += sz.w; h += sz.h; }
    void operator-=(Size sz) { w -= sz.w; h -= sz.h; }
    void operator+=(T val) { w += val; h += val; }
    void operator-=(T val) { w -= val; h -= val; }
    void operator*=(Size sz) { w *= sz.w; h *= sz.h; }
    void operator/=(Size sz) { w /= sz.w; h /= sz.h; }
    void operator*=(T val) { w *= val; h *= val; }
    void operator/=(T val) { w /= val; h /= val; }
};

template <typename T> constexpr bool operator==(Size<T> const &s1, Size<T> const &s2) { return s1.w == s2.w and s1.h == s2.h; }
template <typename T> constexpr bool operator!=(Size<T> const &s1, Size<T> const &s2) { return s1.w != s2.w or s1.h != s2.h; }
template <typename T> constexpr bool operator< (Size<T> const &s1, Size<T> const &s2) { return s1.area() < s2.area(); }
template <typename T> constexpr auto operator- (Size<T> const &s1) { return Size<T>(-s1.w, -s1.h); }
template <typename T> constexpr auto operator- (Size<T> const &s1, Size<T> const &s2) { return Size<T>(s1.w - s2.w, s1.h - s2.h); }
template <typename T> constexpr auto operator- (Size<T> const &s1, T a) { return Size<T>(s1.w - a, s1.h - a); }
template <typename T> constexpr auto operator+ (Size<T> const &s1, Size<T> const &s2) { return Size<T>(s1.w + s2.w, s1.h + s2.h); }
template <typename T> constexpr auto operator+ (Size<T> const &s1, T a) { return Size<T>(s1.w + a, s1.h + a); }
template <typename T> constexpr auto operator/ (Size<T> const &s1, Size<T> const &s2) { return Size<T>(s1.w / s2.w, s1.h / s2.h); }
template <typename T> constexpr auto operator/ (Size<T> const &s1, T a) { return Size<T>(s1.w / a, s1.h / a); }
template <typename T> constexpr auto operator* (Size<T> const &s1, Size<T> const &s2) { return Size<T>(s1.w * s2.w, s1.h * s2.h); }
template <typename T> constexpr auto operator* (Size<T> const &s1, T a) { return Size<T>(s1.w * a, s1.h * a); }
template <typename T> constexpr auto operator+ (Point<T> const &p, Size<T> const &s) { return Point<T>(p.x + s.w, p.y + s.h); }
template <typename T> constexpr auto operator- (Point<T> const &p, Size<T> const &s) { return Point<T>(p.x - s.w, p.y - s.h); }
template <typename T> constexpr auto operator% (Point<T> const &p, Size<T> const &s) { return Point<T>(p.x % s.w, p.y % s.h); }
template <typename T> constexpr auto operator/ (Point<T> const &p, Size<T> const &s) { return Point<T>(p.x / s.w, p.y / s.h); }
template <typename T> constexpr auto operator* (Point<T> const &p, Size<T> const &s) { return Point<T>(p.x * s.w, p.y * s.h); }

template <typename T> constexpr auto distance(Point<T> const &p1, Point<T> const &p2) {
    return Size<T>(p1.x - p2.x, p1.y - p2.y);
}

template <typename T> auto abs(Size<T> const &s) {
    return Size<T>(std::abs(s.w), std::abs(s.h));
}

template <typename T = size_t, 
	  template <typename> class Point_type = Point,
          template <typename> class Size_type = Size>
struct Rect {
    Point_type<T> origin;
    Size_type<T> size;

    constexpr Rect() noexcept = default;
    constexpr Rect(Size_type<T> const &s) noexcept : size(s) {}
    constexpr Rect(Point_type<T> const &o, Size_type<T> const &s) noexcept: origin(o), size(s) {}
    constexpr Rect(T x, T y, T w, T h) noexcept : origin(x, y), size(w, h) {}
    template <typename U> constexpr explicit Rect(Rect<U> const &r): origin(r.origin), size(r.size) {}

    constexpr T min_x() const noexcept { return origin.x; }
    constexpr T max_x() const noexcept { return origin.x + size.w; }
    constexpr T min_y() const noexcept { return origin.y; }
    constexpr T max_y() const noexcept { return origin.y + size.h; }
    constexpr T  area() const noexcept { return size.w * size.h; }

    constexpr Point_type<T>  bottom_left() const noexcept { return Point_type<T>(min_x(), min_y()); }
    constexpr Point_type<T>     top_left() const noexcept { return Point_type<T>(min_x(), max_y()); }
    constexpr Point_type<T> bottom_right() const noexcept { return Point_type<T>(max_x(), min_y()); }
    constexpr Point_type<T>    top_right() const noexcept { return Point_type<T>(max_x(), max_y()); }

    constexpr bool contains_point(Point_type<T> pt) const noexcept {
        return pt.x >= min_x() and pt.x <= max_x() and pt.y >= min_y() and pt.y <= max_y();
    }
    constexpr bool contains_rect(Rect const &r2) const noexcept {
        return this->contains_point(r2.bottomLeft()) and
               this->contains_point(r2.topRight() - Point_type<T>(1, 1));
    }
    constexpr auto inset(Size_type<T> sz) const noexcept {
        return Rect(Point_type<T>(origin.x + sz.w, origin.y + sz.h),
                    Size_type<T>(size.w - sz.w * 2, size.h - sz.h * 2));
    }
    constexpr Rect translated(Size_type<T> amt) const noexcept {
        return Rect(origin.translated(amt), size);
    }
    constexpr Rect centered(Point_type<T> pt) const noexcept {
        return Rect(Point<T>(pt.x - size.w / 2, pt.y - size.h / 2), size);
    }
    constexpr Rect clipped(Rect const &bounds) const noexcept {
        Rect Rect = *this;
        if (Rect.min_x() < bounds.min_x()) { Rect.origin.x = bounds.min_x(); }
        if (Rect.min_y() < bounds.min_y()) { Rect.origin.y = bounds.min_y(); }
        if (Rect.max_x() > bounds.max_x()) { Rect.size.w = bounds.max_x() - Rect.origin.x - 1; }
        if (Rect.max_y() > bounds.max_y()) { Rect.size.h = bounds.max_y() - Rect.origin.y - 1; }
        return Rect;
    }
};

template <typename T, 
	  template <typename> class Point_type = Point,
          template <typename> class Size_type = Size>
constexpr bool operator==(Rect<T, Point_type, Size_type> const &r1,
                          Rect<T, Point_type, Size_type> const &r2) noexcept {
    return r1.origin == r2.origin and r1.size == r2.size;
}

template <typename T, 
	  template <typename> class Point_type = Point,
          template <typename> class Size_type = Size>
constexpr bool operator!=(Rect<T, Point_type, Size_type> const &r1,
                          Rect<T, Point_type, Size_type> const &r2) noexcept {
    return r1.origin != r2.origin or r1.size != r2.size;
}

template <typename T>
constexpr bool Size<T>::contains_point(const Point<T> p) const noexcept {
    return Rect<T>(*this).contains_point(p);
}

template <typename T, 
	  template <typename> class Point_type = Point,
          template <typename> class Size_type = Size>
auto abs(Rect<T, Point_type, Size_type> const &r) {
    return Rect<T, Point_type, Size_type>(abs(r.origin), abs(r.size));
}

template <typename T, 
	  template <typename> class Point_type = Point,
          template <typename> class Size_type = Size>
struct Rect_iter : std::iterator<std::forward_iterator_tag, Point_type<T>,
                                 size_t, Point_type<T> *, Point_type<T> &> {
    Rect<T, Point_type, Size_type> rect;
    Point_type<T> location;

    constexpr Rect_iter() noexcept = default;
    constexpr Rect_iter(Rect<T, Point_type, Size_type> const &r) noexcept: rect(r), location(r.origin.x, r.origin.y) {}
    constexpr Rect_iter(Size_type<T> const &s) noexcept: rect(Rect<T>(Point_type<T>(), s)), location(0, 0) {}
    constexpr Rect_iter(Rect<T, Point_type, Size_type> const &r, Point_type<T> p) noexcept: rect(r), location(p) {}
    constexpr Rect_iter(Rect_iter const &) = default;
    Rect_iter &operator=(Rect_iter const &) = default;

    constexpr Point_type<T> const &operator* () const noexcept { return location; }
    constexpr Point_type<T> const *operator->() const noexcept { return &location; }
    constexpr bool operator==(Rect_iter const &i) const noexcept { return rect == i.rect and location == i.location; }
    constexpr bool operator!=(Rect_iter const &i) const noexcept { return rect != i.rect or location != i.location; }
    constexpr Rect_iter &operator++() noexcept {
        next();
        return *this;
    }
    constexpr Rect_iter operator++(int) noexcept {
        const auto r = *this;
        next();
        return r;
    }

private:
    constexpr void next() noexcept {
        if (++location.x >= rect.max_x()) {
            location.x = rect.min_x();
            ++location.y;
        }
    }
};

template <typename T, 
	  template <typename> class Point_type = Point,
          template <typename> class Size_type = Size>
auto begin(Rect<T, Point_type, Size_type> const &r) noexcept {
    return Rect_iter<T, Point_type, Size_type>(r);
}

template <typename T, 
	  template <typename> class Point_type = Point,
          template <typename> class Size_type = Size>
auto end(Rect<T, Point_type, Size_type> const &r) noexcept {
    return Rect_iter<T, Point_type, Size_type>(r, r.top_left());
}

}
#endif
