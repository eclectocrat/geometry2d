# geometry2d

Simpl(istic) C++17 single header library for basic 2D geometry with points, sizes, and rects.

Copyright(C) Jeremy Jurksztowicz 2020. All rights reserved.

### See geometry2d.hpp for details and documentation.
